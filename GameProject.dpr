program GameProject;

uses
  Windows,
  Messages,
  opengl,
  GameEngine in 'GameEngine.pas',
  dialogs;

var
  Msg        : TMSG;
  LWndClass  : TWndClass;
  hMainHandle: HWND;
  gameEng : TGameEngine;

procedure ReleaseResources;
begin
  PostQuitMessage(0);
end;

function WindowProc(hWnd,Msg:Longint; wParam : WPARAM; lParam: LPARAM):Longint; stdcall;
begin
  case Msg of
      WM_DESTROY: ReleaseResources;
  end;
  Result:=DefWindowProc(hWnd,Msg,wParam,lParam);
end;

begin
  //create the window
  LWndClass.hInstance := hInstance;
  with LWndClass do
    begin
      lpszClassName := 'MyWinApiWnd';
      Style         := CS_HREDRAW or CS_VREDRAW or CS_OWNDC;
      hIcon         := LoadIcon(hInstance,'MAINICON');
      lpfnWndProc   := @WindowProc;
      hbrBackground := COLOR_BTNFACE+1;
      hCursor       := LoadCursor(0,IDC_ARROW);
    end;

  RegisterClass(LWndClass);
  hMainHandle := CreateWindow(LWndClass.lpszClassName, 'GameEngine', WS_CAPTION or WS_VISIBLE or WS_BORDER or WS_SYSMENU, 0,
      0, GetSystemMetrics(SM_CXSCREEN) div 2, GetSystemMetrics(SM_CYSCREEN) div 2, 0, 0, hInstance, nil);

  gameEng := TGameEngine.Create(hMainHandle);

  while True do
  begin
    gameEng.DrawScene;
    if PeekMessage(msg, 0, 0, 0, PM_REMOVE) then begin
      if msg.message = WM_QUIT then
        break
      else begin
        TranslateMessage(Msg);
        DispatchMessage(Msg);
      end;
    end;
  end;
end.
