unit GameEngine;

interface

uses opengl, windows, Contnrs, classes;

type
  TVector = record
    x, y, z : GLfloat;
  end;

  IDrawable = interface
    procedure Draw;
  end;

  TGameEngine = class(TObject)
    public
      constructor Create(Window : HWND);
      procedure DrawScene;
    private
      WindowHandle : HWND;
      DC : HDC;
      RC : HGLRC;
      obj : TObject;
      procedure InitGL;
      class procedure setupPixelFormat(DC:HDC);
  end;

  TGameObject = class(TInterfacedObject, IDrawable)
    public
      Name : string;
      Position : TVector; //position
      Rotation : TVector; //rotation
      Vertexes : TList;
      constructor Create(Name : string; x : GLfloat = 0; y : GLfloat = 0;
        z : GLfloat = 0; rX : GLfloat = 0; rY : GLfloat = 0; rZ : GLfloat = 0);
      procedure Draw;
  end;


  TScene = class(TInterfacedObject, IDrawable)
    public
      procedure Draw;
      procedure AddObject(Obj : TGameObject);
    private
      Objects : TObjectList;
  end;

implementation

{ TGameEngine }

constructor TGameEngine.Create(Window : HWND);
var v : TVector;
begin
  WindowHandle := Window;
  dc := GetDC(WindowHandle);
  SetupPixelFormat(DC);
  RC := wglCreateContext(DC);
  wglMakeCurrent(DC, RC);
  obj := TGameObject.Create('lol');
  v.x := 0;
  v.y := 1
  (obj as TGameObject).Vertexes.Add()
  InitGL;
end;

procedure TGameEngine.DrawScene;
begin
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glLoadIdentity;

  glTranslatef(0, 0, -20);
  SwapBuffers(wglGetCurrentDC);
end;

procedure TGameEngine.InitGL;
begin
  // set viewing projection
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  gluPerspective(45, (GetSystemMetrics(SM_CXSCREEN) div 2) /
      (GetSystemMetrics(SM_CYSCREEN) div 2), 0.1, 100);
  // position viewer
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;

  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
end;

class procedure TGameEngine.setupPixelFormat(DC: HDC);
const
   pfd:TPIXELFORMATDESCRIPTOR = (
        nSize:sizeof(TPIXELFORMATDESCRIPTOR);	// size
        nVersion:1;			// version
        dwFlags:PFD_SUPPORT_OPENGL or PFD_DRAW_TO_WINDOW or
                PFD_DOUBLEBUFFER;	// support double-buffering
        iPixelType:PFD_TYPE_RGBA;	// color type
        cColorBits:24;			// preferred color depth
        cRedBits:0; cRedShift:0;	// color bits (ignored)
        cGreenBits:0;  cGreenShift:0;
        cBlueBits:0; cBlueShift:0;
        cAlphaBits:0;  cAlphaShift:0;   // no alpha buffer
        cAccumBits: 0;
        cAccumRedBits: 0;  		// no accumulation buffer,
        cAccumGreenBits: 0;     	// accum bits (ignored)
        cAccumBlueBits: 0;
        cAccumAlphaBits: 0;
        cDepthBits:16;			// depth buffer
        cStencilBits:0;			// no stencil buffer
        cAuxBuffers:0;			// no auxiliary buffers
        iLayerType:PFD_MAIN_PLANE;  	// main layer
   bReserved: 0;
   dwLayerMask: 0;
   dwVisibleMask: 0;
   dwDamageMask: 0;                    // no layer, visible, damage masks
   );
var pixelFormat:integer;
begin
   pixelFormat := ChoosePixelFormat(DC, @pfd);
   if (pixelFormat = 0) then
        exit;
   if (SetPixelFormat(DC, pixelFormat, @pfd) <> TRUE) then
        exit;
end;

{ TGameObject }

constructor TGameObject.Create(Name: string; x, y, z, rX, rY, rZ: GLfloat);
begin
  name := Name;
  Position.x := x;
  Position.y := y;
  Position.z := z;
  Rotation.x := rX;
  Rotation.y := rY;
  Rotation.z := rZ;
  Vertexes := TList.Create;
end;

procedure TGameObject.Draw;
var i : ^TVector;
begin
  glBegin(GL_TRIANGLES);
  for i in Vertexes do
    glVertex3f(i.x, i.y, i.z);
  glEnd;
end;

{ TScene }

procedure TScene.AddObject(Obj: TGameObject);
begin
  Objects.Add(Obj);
end;

procedure TScene.Draw;
begin

end;

end.
